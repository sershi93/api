<?php

use Illuminate\Http\Request;

Route::group(['namespace' => 'V1', 'prefix' => 'v1'], function () {

    Route::group(['prefix' => 'task'], function () {
        Route::get('/', 'TaskController@tasks');
        Route::get('/{id}', 'TaskController@task');
    });
});
