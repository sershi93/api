<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Cache;

class Task extends Model
{
    CONST MINUTES = 60;

    protected $fillable = [
        'id',
        'title',
        'date',
    ];

    public static function getTasks()
    {
        if (!Cache::get('task')) self::makeTasks();

        return Cache::get('task');
    }

    public static function detail($id)
    {
        if (!Cache::get('task')) self::makeTasks();
        $item = array_get(Cache::get('task'), $id);

        if ($item) {
            $item->author = "Автор {$item->id}";
            $item->status = "Статус {$item->id}";
            $item->description = "Описание {$item->id}";
        }
        return $item;
    }

    private static function makeTasks()
    {
        Cache::remember('task', self::MINUTES, function () {
            $date = Carbon::now();
            $tasks = [];
            for ($i = 1; $i <= 1000; $i++) {
                $tasks[$i] = new Task([
                    'id' => $i,
                    'title' => "Заголовок $i",
                    'date' => $date->copy()->addHours($i - 1)->toIso8601String()
                ]);
            }

            return $tasks;
        });
    }
}
