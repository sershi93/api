<?php

namespace App\Http\Controllers\V1;

use App\Models\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    public function tasks()
    {
        return Task::getTasks();
    }

    public function task($id)
    {
        return Task::detail($id);
    }
}
